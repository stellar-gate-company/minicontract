// SPDX-License-Identifier: MIT
pragma solidity 0.8.19;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * @title MiniTAZZ
 * @dev A contract for managing a game and upgrades with ERC20 tokens
 */
contract MiniTAZZ is Ownable {
    using SafeERC20 for IERC20;

    /**
     * @dev The admin address
     */
    address public admin;

    /**
     * @dev The TAZZ token contract
     */
    IERC20 public tazzToken;

    /**
     * @dev Total TAZZ tokens in contract
     */
    uint256 public totalTAZZ;

    /**
     * @dev Admin rewards
     */
    uint256 public adminRewards;

    /**
     * @dev Total TAZZ tokens minus admin rewards
     */
    uint256 public totalTAZZMinusAdmin;

    /**
     * @dev Admin commission rate (7%)
     */
    uint256 public constant ADMIN_COMMISSION_RATE = 7;

    /**
     * @dev Number of decimals in the token
     */
    uint8 public constant DECIMALS = 18;

    /**
     * @dev Upgrade cost (1 TAZZ)
     */
    uint256 public constant UPGRADE_COST = 1 * 10**(DECIMALS - 2);

    /**
     * @dev Base attack damage (1)
     */
    uint256 public constant BASE_ATTACK_DAMAGE = 1;

    /**
     * @dev Start battle time (0)
     */
    uint256 public startBattleTime;

    /**
     * @dev Stop battle time (1)
     */
    uint256 public stopBattleTime;

    /**
     * @dev Current version
     */
    uint256 public currentVersion;

    /**
     * @dev Highest version
     */
    uint256 public highestVersion;

    /**
     * @dev Mapping of user to spider tank manufactured
     */
    mapping(address => mapping(uint256 => uint256)) public spiderTankManufactured;

    /**
     * @dev Mapping of HQ to spider tank assigned
     */
    mapping(uint256 => mapping(uint256 => uint256)) public spiderTankAssigned;

    /**
     * @dev Mapping of user to HQ attack rate
     */
    mapping(address => mapping(uint256 => uint256)) public hqAttackRate;

    /**
     * @dev Mapping of user to HQ attack damage
     */
    mapping(address => mapping(uint256 => uint256)) public hqAttackDmg;

    /**
     * @dev Mapping of HQ to health
     */
    mapping(uint256 => mapping(uint256 => uint256)) public hqHealth;

    /**
     * @dev Mapping of user to HQ health
     */
    mapping(address => mapping(uint256 => uint256)) public hqHealthUser;

    /**
     * @dev Mapping of HQ to contributions
     */
    mapping(uint256 => mapping(uint256 => uint256)) public hqContributions;

    /**
     * @dev Mapping of user to contributions
     */
    mapping(address => mapping(uint256 => uint256)) public userContributions;

    /**
     * @dev Mapping of user to HQ
     */
    mapping(address => mapping(uint256 => uint256)) public userHQ;

    /**
     * @dev Mapping of user to claimable rewards
     */
    mapping(address => mapping(uint256 => uint256)) public claimableRewards;

    /**
     * @dev Mapping of user to upgrades
     */
    mapping(address => mapping(uint256 => uint256)) public userUpgrades;

    /**
     * @dev Mapping of user to TAZZ balance
     */
    mapping(address => uint256) public userTAZZBalance;

    /**
     * @dev Mapping of user to auto-join status
     */
    mapping(address => bool) public autoJoinEnabled;

    /**
     * @dev Mapping of user to auto-upgrade status
     */
    mapping(address => bool) public autoUpgradeEnabled;

    /**
     * @dev Mapping of version to winning HQ
     */
    mapping(uint256 => uint256) public winningHQPerVersion;

    /**
     * @dev Array of auto-join users
     */
    address[] public autoJoinUsers;

    /**
     * @dev Event emitted when TAZZ is stored
     */
    event TAZZStored(address indexed user, uint256 amount);

    /**
     * @dev Event emitted when TAZZ is withdrawn
     */
    event TAZZWithdrawn(address indexed user, uint256 amount);

    /**
     * @dev Event emitted when auto-join status is updated
     */
    event AutoJoinStatusUpdated(address indexed user, bool enabled);

    /**
     * @dev Event emitted when auto-upgrade status is updated
     */
    event AutoUpgradeStatusUpdated(address indexed user, bool enabled);

    /**
     * @dev Constructor
     * @param adminAddress The admin address
     * @param tazzTokenAddress The TAZZ token contract address
     */
    constructor(address adminAddress, address tazzTokenAddress) {
        require(adminAddress!= address(0), "Invalid admin address");
        require(tazzTokenAddress!= address(0), "Invalid token address");

        admin = adminAddress;
        tazzToken = IERC20(tazzTokenAddress);
        adminRewards = 0;
        totalTAZZ = 0;
        totalTAZZMinusAdmin = 0;
        currentVersion = 1;
        highestVersion = 1;
    }

    /**
     * @dev Clear the map and reset the state
     */
    function clearMap() external onlyOwner {
        currentVersion++;
        highestVersion = currentVersion;

        for (uint256 i = 0; i < autoJoinUsers.length; i++) {
            address user = autoJoinUsers[i];
            uint256 randomHQ = uint256(keccak256(abi.encodePacked(block.timestamp, user))) % 4 + 1;
            userHQ[user][currentVersion] = randomHQ;
            spiderTankManufactured[user][currentVersion] = 1;
            spiderTankAssigned[randomHQ][currentVersion] += 1;

            if (autoUpgradeEnabled[user]) {
                upgradeSpiderTankAuto(user);
            }
        }
    }

    /**
     * @dev Upgrade spider tank automatically
     * @param user The user address
     */
    function upgradeSpiderTankAuto(address user) internal {
        uint256 tokenBalance = userTAZZBalance[user];
        require(tokenBalance >= UPGRADE_COST + ADMIN_COMMISSION_RATE, "Insufficient balance for upgrade and fees");

        userTAZZBalance[user] -= (UPGRADE_COST + ADMIN_COMMISSION_RATE);
        adminRewards += ADMIN_COMMISSION_RATE;
        totalTAZZ += UPGRADE_COST;

        if (uint256(keccak256(abi.encodePacked(block.timestamp, user))) % 2 == 0) {
            hqAttackRate[user][currentVersion] += 1;
        } else {
            hqAttackDmg[user][currentVersion] += 1;
        }

        userUpgrades[user][currentVersion]++;
    }

    /**
     * @dev Store TAZZ tokens
     * @param amount The amount of TAZZ tokens to store
     */
    function storeTAZZ(uint256 amount) external {
        require(amount > 0, "Amount must be greater than 0");

        tazzToken.safeTransferFrom(msg.sender, address(this), amount);
        userTAZZBalance[msg.sender] += amount;

        emit TAZZStored(msg.sender, amount);
    }

    /**
     * @dev Withdraw TAZZ tokens
     * @param amount The amount of TAZZ tokens to withdraw
     */
    function withdrawTAZZ(uint256 amount) external {
        require(amount > 0, "Amount must be greater than 0");
        require(userTAZZBalance[msg.sender] >= amount, "Insufficient balance");

        userTAZZBalance[msg.sender] -= amount;
        tazzToken.safeTransfer(msg.sender, amount);

        emit TAZZWithdrawn(msg.sender, amount);
    }

    /**
     * @dev Set auto-join status
     * @param enabled The auto-join status
     */
    function setAutoJoin(bool enabled) external {
        if (enabled) {
            autoJoinUsers.push(msg.sender);
        } else {
            // Remove user from autoJoinUsers array
            for (uint256 i = 0; i < autoJoinUsers.length; i++) {
                if (autoJoinUsers[i] == msg.sender) {
                    autoJoinUsers[i] = autoJoinUsers[autoJoinUsers.length - 1];
                    autoJoinUsers.pop();
                    break;
                }
            }
        }
        autoJoinEnabled[msg.sender] = enabled;
        emit AutoJoinStatusUpdated(msg.sender, enabled);
    }

    /**
     * @dev Set auto-upgrade status
     * @param enabled The auto-upgrade status
     */
    function setAutoUpgrade(bool enabled) external {
        autoUpgradeEnabled[msg.sender] = enabled;
        emit AutoUpgradeStatusUpdated(msg.sender, enabled);
    }

    /**
     * @dev Choose spider tank HQ
     * @param hqNumber The HQ number
     */
    function chooseSpiderTankHQ(uint256 hqNumber) external {
        require(startBattleTime == 1, "Match has not started yet");
        require(userHQ[msg.sender][currentVersion] == 0, "User has already chosen an HQ");
        require(hqNumber >= 1 && hqNumber <= 4, "Invalid HQ number");

        userHQ[msg.sender][currentVersion] = hqNumber;
        spiderTankManufactured[msg.sender][currentVersion] = 1;
        spiderTankAssigned[hqNumber][currentVersion] += 1;
    }

    /**
     * @dev Upgrade spider tank
     * @param upgradeType The upgrade type (rate or damage)
     */
    function upgradeSpiderTank(string memory upgradeType) external {
        require(startBattleTime == 1, "Match has not started yet");
        require(userHQ[msg.sender][currentVersion]!= 0, "Choose an HQ first");
        require(userUpgrades[msg.sender][currentVersion] < 2, "Upgrade limit reached");

        uint256 tokenBalance = tazzToken.balanceOf(msg.sender);
        require(tokenBalance >= UPGRADE_COST + ADMIN_COMMISSION_RATE, "Insufficient balance for upgrade and fees");

        tazzToken.safeTransferFrom(msg.sender, address(this), UPGRADE_COST);
        tazzToken.safeTransferFrom(msg.sender, admin, ADMIN_COMMISSION_RATE);

        if (keccak256(abi.encodePacked(upgradeType)) == keccak256(abi.encodePacked("rate"))) {
            hqAttackRate[msg.sender][currentVersion] += 1;
        } else if (keccak256(abi.encodePacked(upgradeType)) == keccak256(abi.encodePacked("damage"))) {
            hqAttackDmg[msg.sender][currentVersion] += 1;
        } else {
            revert("Invalid upgrade type");
        }

        totalTAZZ += UPGRADE_COST;
        userUpgrades[msg.sender][currentVersion]++;
    }

    /**
     * @dev Claim reward
     */
    function claimReward() external {
        require(stopBattleTime == 1, "Match has not ended yet");

        uint256 totalReward = 0;

        for (uint256 version = 1; version <= currentVersion; version++) {
            if (winningHQPerVersion[version] == 0) {
                continue;
            }

            if (claimableRewards[msg.sender][version] == 0) {
                continue;
            }

            uint256 userContribution = userContributions[msg.sender][version];
            if (userContribution == 0) {
                continue;
            }

            if (userHQ[msg.sender][version]!= winningHQPerVersion[version]) {
                continue;
            }

            uint256 totalHQContribution = hqContributions[winningHQPerVersion[version]][version];
            uint256 reward = (userContribution * totalTAZZMinusAdmin) / totalHQContribution;

            if (reward > totalTAZZMinusAdmin) {
                reward = totalTAZZMinusAdmin;
            }

            userContributions[msg.sender][version] = 0;
            claimableRewards[msg.sender][version] = 0; // Mark the version as claimed
            totalReward += reward;
            totalTAZZMinusAdmin -= reward;
        }

        require(totalReward > 0, "No rewards to claim");
        tazzToken.safeTransfer(msg.sender, totalReward);
    }

    /**
     * @dev Battle report
     * @param winningHQnumber The winning HQ number
     */
    function battleReport(uint256 winningHQnumber) external onlyOwner {
        require(stopBattleTime == 1, "Match has not ended yet");
        winningHQPerVersion[currentVersion] = winningHQnumber;
    }

    /**
     * @dev Start battle
     */
    function startBattle() external onlyOwner {
        require(stopBattleTime == 1, "Previous battle has not ended yet");
        
        currentVersion++;
        highestVersion = currentVersion;
        startBattleTime = 1;
        stopBattleTime = 0;
    }

    /**
     * @dev Stop battle
     */
    function stopBattle() external onlyOwner {
        startBattleTime = 0;
        stopBattleTime = 1;
    }

    /**
     * @dev Check if battle is active
     * @return True if battle is active, false otherwise
     */
    function isBattleActive() external view returns (bool) {
        return startBattleTime == 1 && stopBattleTime == 0;
    }

    /**
     * @dev Get player stats
     * @param user The user address
     * @return User stats (attack rate, attack damage, tanks manufactured, contributions, HQ, health)
     */
    function playerStats(address user) external view returns (uint256, uint256, uint256, uint256, uint256, uint256) {
        return (
            hqAttackRate[user][currentVersion],
            hqAttackDmg[user][currentVersion],
            spiderTankManufactured[user][currentVersion],
            userContributions[user][currentVersion],
            userHQ[user][currentVersion],
            hqHealthUser[user][currentVersion]
        );
    }

    /**
     * @dev Get player stats across all versions
     * @param user The user address
     * @return Total user stats (attack rate, attack damage, tanks manufactured, contributions, health)
     */
    function playerStatsAcrossAllVersions(address user) external view returns (uint256, uint256, uint256, uint256, uint256) {
        uint256 rate = 0;
        uint256 dmg = 0;
        uint256 tanks = 0;
        uint256 contributions = 0;
        uint256 health = 0;

        for (uint256 version = 1; version <= highestVersion; version++) {
            rate += hqAttackRate[user][version];
            dmg += hqAttackDmg[user][version];
            tanks += spiderTankManufactured[user][version];
            contributions += userContributions[user][version];
            health += hqHealthUser[user][version];
        }

        return (rate, dmg, tanks, contributions, health);
    }
}